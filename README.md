Wiegand data reader with Raspberry Pi, tested with RPi Model B rev2, and a 26bit Wiegand Card reader.

- Implementation is interrupt driven, no polling occurs. It works like so:
    - After each bit is read, a timeout is set for the next bit
    - If timeout is reached read code is evaluated for correctness using the 2 wiegand parity bits
    - After a code is read, wiegand_timeout() is called, which calls show_code() to display the read wiegand code.

Debugging is supported, with timing info


```
#!text

Wiegand Bits:
pFFFFFFFFNNNNNNNNNNNNNNNNP
p: even parity of F-bits and leftmost 4 N-bits
F: Facility code
N: Card Number
P: odd parity of rightmost 12 N-bits

```

Compile with: gcc wiegand_rpi.c   -lwiringPi -lpthread -lrt  -Wall -o wiegand_rpi -O


```
#!text

Wiegand Read Agent (https://bitbucket.org/sivann/wiegand_read)
Usage: wiegand_read [-d] [-0 data 0pin] [-1 data1 pin]
        -d              debug
        -0              GPIO pin for data0 pulse
        -1              GPIO pin for data1 pulse
        Check http://wiringpi.com/pins for WiringPi pin numbers

```


Example output with debugging (run with -d). The "Full code" matches the number printed on the plastic key.

```
#!text

Bit:00, Pulse 1, 3654473 us since last bit
Bit:01, Pulse 1, 2306 us since last bit
Bit:02, Pulse 0, 2344 us since last bit
Bit:03, Pulse 0, 2325 us since last bit
Bit:04, Pulse 0, 2340 us since last bit
Bit:05, Pulse 1, 2317 us since last bit
Bit:06, Pulse 1, 2343 us since last bit
Bit:07, Pulse 1, 2322 us since last bit
Bit:08, Pulse 0, 2374 us since last bit
Bit:09, Pulse 1, 2323 us since last bit
Bit:10, Pulse 1, 2315 us since last bit
Bit:11, Pulse 1, 2327 us since last bit
Bit:12, Pulse 0, 2346 us since last bit
Bit:13, Pulse 1, 2349 us since last bit
Bit:14, Pulse 1, 2305 us since last bit
Bit:15, Pulse 0, 2380 us since last bit
Bit:16, Pulse 0, 2295 us since last bit
Bit:17, Pulse 1, 2372 us since last bit
Bit:18, Pulse 1, 2303 us since last bit
Bit:19, Pulse 0, 2329 us since last bit
Bit:20, Pulse 0, 2340 us since last bit
Bit:21, Pulse 0, 2391 us since last bit
Bit:22, Pulse 0, 2271 us since last bit
Bit:23, Pulse 0, 2327 us since last bit
Bit:24, Pulse 1, 2344 us since last bit
Bit:25, Pulse 0, 2318 us since last bit
wiegand_timeout()

*** Code Valid: : 1
*** Facility code: 142(dec) 0x8E
*** Card code: 60609(dec) 0xECC1
*** Full code: 9366721
*** P0:1 P1:0

```
