CC=gcc
CFLAGS=-I. -Wall -I/usr/local/include/hiredis -O
DEPS = wiegand_rpi.h
DEPS = 
LIBS = -lwiringPi -lpthread -lrt 
LIBSR = -lhiredis
OBJ = wiegand_rpi.o 

binaries=wiegand_rpi redis_subscribe redis_publish

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

wiegand_rpi: $(OBJ)
	gcc -o $@ $^ $(CFLAGS) $(LIBS)

redis: redis_subscribe redis_publish

redis_subscribe: redis_subscribe.o
	gcc -o $@ $^ $(CFLAGS) $(LIBS) $(LIBSR)

redis_publish: redis_publish.o
	gcc -o $@ $^ $(CFLAGS) $(LIBS) $(LIBSR)

all: $(binaries)

clean:
	rm -f *.o $(binaries)
